const path = require('path');
const express = require('express');
const exphbs = require('express-handlebars');
const intel = require('intel');
const app = express();

const port = 8000;

// app.use((request, response, next) => {
//   console.log(request.headers);
//   next();
// });
//
// app.use((request, response, next) => {
//   request.chance = Math.random();
//   next();
// });

app.get('/', (request, response) => {
  response.render('authentication');
});

app.engine('.hbs', exphbs({
  defaultLayout: 'main',
  extname: '.hbs',
  layoutsDir: path.join(__dirname, '../views/layouts')
}));

app.set('view engine', '.hbs');

app.set('views', path.join(__dirname, '../views'));

app.use((err, request, response) => {
  intel.critical(err);
  response.status(500).send('Something broke!');
});

app.listen(port, (err) => {
  if (err) {
    return intel.critical('something bad happened', err);
  }
  intel.info(`server is listening on ${port}`);
});