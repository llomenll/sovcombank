module.exports = {
  "extends": "defaults/configurations/google",
  "parser": "babel-eslint",
  "env": {
  "browser": true,
    "jquery": true,
    "node": true,
    "es6": true,
    "mocha": true,
    "phantomjs": true
  },
    "globals": {
    "__DEV__": true,
      "__SERVER__": true
  },
    "parserOptions": {
    "ecmaFeatures": {
      "jsx": true,
        "modules": true
    }
  },
    "rules": {
    "dot-location": [2, "property"],
      "no-extra-parens": [2, "functions"],
      "no-unused-vars": 2,
      "no-console": 2,
      "no-invalid-this": 0,
      "no-const-assign": 2,
      "prefer-const": 2,
      "no-var": 2,
      "prefer-template": 2,
      "prefer-arrow-callback": 2
  }
};